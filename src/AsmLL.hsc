module AsmLL where

import BasePrelude
import Foreign.C.Types
import System.Posix.Types

#include <sys/mman.h>

foreign import ccall unsafe "mmap"
    mmap :: Ptr a -> CSize -> CInt -> CInt -> CInt -> COff -> IO (Ptr a)

foreign import ccall unsafe "mprotect"
    mprotect :: Ptr a -> CSize -> CInt -> IO CInt

foreign import ccall unsafe "munmap"
    munmap :: Ptr a -> CSize -> IO ()

foreign import ccall unsafe "cpu_flags"
    cpu_flags :: IO Int

-- | Allocates a huge buffer for code generation (it's only virtual
-- memory, so doesn't cost anything), runs the first continuation to
-- fill it, changes permission so it can be executed, runs the second
-- continuation, and cleans up.
withBuf :: (Ptr Word8 -> CSize -> IO x) -> (Ptr Word8 -> CSize -> x -> IO y) -> IO y
withBuf k_compile k_execute = do
    let buf_size =  1024 * 1024 * 1024
    bracket (mmap nullPtr buf_size
                  (#const PROT_READ | PROT_WRITE)
                  (#const MAP_ANONYMOUS | MAP_PRIVATE)
                  (-1) 0)
            (\ptr -> munmap ptr buf_size)
            (\ptr -> do x <- k_compile ptr buf_size
                        _ <- mprotect ptr buf_size (#const PROT_READ | PROT_EXEC)
                        k_execute ptr buf_size x)
