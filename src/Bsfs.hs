-- | Here we will apply likelihoods from a generating function to a lie:
-- a Neandertard that is so good that we can call genotypes.
--
-- We will read exactly four heffalumps, merge them into variant calls,
-- split them into blocks such that they have the same number of
-- informative sites, extract their bSFSs, compose them into a
-- likelihood function, and get the MLE.
--
-- Note that blocks have the same number of /informative sites/, but
-- their physical size can vary.  This way, they all have the same
-- expected number of mutations, the bSFSs of the whole genome can be
-- tabulated, and the likelihood function becomes simpler.

module Bsfs where

import BasePrelude                  hiding ( Sum )
import Bio.Heffa.Genome
import Bio.Heffa.Lump
import Control.Monad.Primitive             ( PrimMonad )
import GenFunc
import Numeric.AD                          ( conjugateGradientAscent )
import Numeric.AD.Mode                     ( isKnownZero )
import Options.Applicative
import Streaming
import System.IO                           ( hFlush, stdout )

import qualified Streaming.Prelude                      as Q
import qualified Codec.Serialise                        as Cborg
import qualified Data.ByteString.Lazy                   as L
import qualified Data.HashMap.Strict                    as H
import qualified Data.Vector.Storable                   as W
import qualified Data.Vector.Unboxed.Mutable.Sized      as M
import qualified Data.Vector.Unboxed.Sized              as U
import qualified Neandertard                            as Nea


-- XXX It's probably worth the effort to support shitty neandertard and
-- chimp genomes; i.e. treat them as haploid and use the generating
-- function of a population that has only one gene in the respective
-- demes.
--
-- XXX Does it ever make sense *not* to fold the bSFS?  As in, do we
-- ever know which allele is ancestral?  This would definitely need
-- additional logic, because it's clearly not the reference allele.

data BSFSs = BSFSs { bsfss_num_observed :: Int
                   , bsfss_spectrum :: H.HashMap (BSFS 79{-40-}) Int }
  deriving Generic

instance Cborg.Serialise BSFSs

bsfs_options :: Parser (IO ())
bsfs_options = bsfs_main
    <$> strOption (short 'o' <> long "output" <> metavar "FILE" <> help "Write bsfs to FILE")
    <*> strOption (long "european"    <> metavar "HEF" <> help "Heffalump of \"European\" sample")
    <*> strOption (long "african"     <> metavar "HEF" <> help "Heffalump of \"African\" sample")
    <*> strOption (long "neandertard" <> metavar "HEF" <> help "Heffalump of \"Neandertard\" sample")
    <*> strOption (long "chimpanzee"  <> metavar "HEF" <> help "Heffalump of \"Chimpanzee\" sample")

    <*> optional (strOption (short 'r' <> long "reference" <> metavar "FILE" <> help "Read reference from FILE (.2bit)"))
    <*> option auto (short 'l' <> long "block-length" <> value 300
                               <> metavar "LEN" <> help "Blocks of maximum length LEN")
    <*> option auto (short 'm' <> long "num-observed" <> value 200
                               <> metavar "NUM" <> help "Exactly NUM observed sites per block")
  where
    bsfs_main :: FilePath -> FilePath -> FilePath -> FilePath -> FilePath -> Maybe FilePath -> Int       -> Int         -> IO ()
    bsfs_main    ofile       eur_hef     afr_hef     nea_hef     pan_hef     reference         max_physical num_observed = do
        let partials = applyGenFunc (generatingFunction Nea.memoGeneratingFunction Nea.today)
                                    (initial_model num_observed) (exp . Nea.mutation_rate)
        bsfss <- decodeManyRef reference [eur_hef,afr_hef,nea_hef,pan_hef] $ \ref ->
                 -- Q.fold_ (\acc v -> H.insertWith (+) v (1::Int) acc) H.empty id .

                 Q.mapM_ (\(bsfs,_,_) -> do
                    putStr $ show_bsfs bsfs ++ ": "
                    hFlush stdout
                    let lk = likelihood partials (exp $ Nea.mutation_rate $ initial_model num_observed) bsfs
                    putStrLn $ show lk ++ if isKnownZero lk then " (zero)" else "") .
                 Q.take 1000 .

                 -- progressGen :: MonadLog m => (Int -> a -> String) -> Int -> Q.Stream (Q.Of a) m r -> Q.Stream (Q.Of a) m r

                 mapped block_to_bsfs .
                 -- mapped (fmap (() :>) . Q.print) .
                 -- mapped clump_range .

                 observedBlocks (NumObserved num_observed) (MaxPhysical max_physical) .
                 addRef (MaxHole 49) ref .
                 mergeLumps (NumOutgroups 0)

        pure ()

        -- L.writeFile ofile $ Cborg.serialise (BSFSs num_observed bsfss)

clump_range :: Monad m => Q.Stream (Of Clump) m r -> m (Of (Int,Int,Int) r)
clump_range = inspect >=> \case
    Left r -> pure $ (0,0,0) :> r
    Right (c0 :> cs) -> Q.mapOf (\l -> (c_chr c0, c_pos c0, l + c_pos c0)) <$>
                        Q.fold (\ !acc c -> acc + the_len c) 0 id (Q.cons c0 cs)
  where
    the_len Clump { c_len = l } = l
    the_len Hole  { c_len = l } = l
    the_len Variants {}         = 1

fit_options :: Parser (IO ())
fit_options = fit_main
    <$> strArgument (metavar "BSFS" <> help "File with compiled BSFSs")
  where
    fit_main ifile = do
        BSFSs num_observed bsfss <- Cborg.deserialise <$> L.readFile ifile

        -- XXX this is not at all tested
        -- Possible alternatives include 'gradientAscent', the
        -- CG_Descent method by Hager/Zhang, (L)BFGS.
        -- conjugateGradientAscent is merely the most available
        -- optimizer right now.
        print $ initial_model num_observed
        -- XXX print $ log_lk_genome bsfss (initial_model num_observed)
        -- XXX mapM_ print $ conjugateGradientAscent (log_lk_genome bsfss) (initial_model num_observed)

        -- XXX for industrial strength, the optimization should
        -- terminate at some point
        --
        -- XXX given the MLE, we should get confidence intervals from
        -- the Hessian matrix.  requires a PCA.  suitable code might be
        -- in genomfart

show_bsfs :: BSFS n -> String
show_bsfs (BSFS v) = shows the_total ":\t[" ++ intercalate "," the_list ++ "]"
  where
    the_total = U.sum v

    the_list = [ shows n "*" ++ to_set 0 i
               | (i,n) <- zip [1..] (U.toList v), n > 0 ]

    to_set :: Int -> Int -> String
    to_set _ 0 = []
    to_set i n = case quotRem n 3 of
        (n',0) -> to_set (i+1) n'
        (n',1) -> toLower (label i) : to_set (i+1) n'
        (n',_) -> toUpper (label i) : to_set (i+1) n'

    label :: Int -> Char
    label 0 = 'E'
    label 1 = 'A'
    label 2 = 'N'
    label 3 = 'P'
    label _ = '?'


-- | Computes the bSFS of a block.  We fold the bSFS, because we can't
-- know the identity of the ancestral allele.

block_to_bsfs :: (PrimMonad m, MonadIO m) => Stream (Of Clump) m r -> m (Of (BSFS 79{-40-}, Int, Int) r)
block_to_bsfs = inspect >=> \case
    Left           r ->    pure $ (BSFS (U.replicate 0), 0, 0) :> r
    Right (c0 :> cs) -> do v :> r <- go (Q.cons c0 cs)
                           pure $ (v, c_chr c0, c_pos c0) :> r

  where
    go stream = do v <- M.replicate 0
                   r <- Q.mapM_ (step v) stream
                   (:> r) . BSFS <$> U.unsafeFreeze v

    step v Variants{..} | allObserved c_vars = mapM_ (bump v . snd) c_vars
    step _ _                                 = pure ()

    bump v cs = when (i >= 0 && i /= 80) $ M.unsafeRead v i >>= M.unsafeWrite v i . succ
      where
        t | W.length cs == 4 = W.foldr' (\c acc -> 3 * acc + to_index c) (0::Int) cs
        i = t - 1                       -- without folding
        -- i = min t (80 - t) - 1          -- folded in half

    -- We already discarded sites where any sample has an N.  Now Ns
    -- count as reference---we're counting mutations, after all.
    to_index (AC _ 0) =  0      -- only reference, assume two reference alleles
    to_index (AC 0 _) =  2      -- only alternative, assume two alternative alleles
    to_index (AC _ _) =  1      -- heterozygous, therefore one alternative allele


-- | Maximize the likelihood for a set of genomes.  Takes the table of
-- BFSFs and an initial guess at the 'Nea.ModelParams', returns a stream of
-- increasingly accurate 'Nea.ModelParams'.
--
-- This code contains an important detail:  Under the infinite sites approximation,
-- which we use, some patterns of mutations are impossible.  If at least
-- one block has such a configuration, the likelihood becomes zero, and
-- the log-likelihood becomes negative infinity.  All attempts to
-- optimize it end up in collections of NaNs.  Fortunately, these
-- impossible blocks yield a lkelihood that is so obviously zero, we can
-- detect it cheaply and reliably.

log_lk_genome :: (Floating a, Eq a) => H.HashMap (BSFS 40) Int -> Nea.ModelParams a -> a
log_lk_genome bsfss mp = H.foldlWithKey' step 0 bsfss
  where
    partials = applyGenFuncFolded (generatingFunction Nea.memoGeneratingFunction Nea.today) mp (exp . Nea.mutation_rate)
    lk = likelihood partials (exp $ Nea.mutation_rate mp)
    step acc bsfs num = if lk bsfs == 0 then acc else acc + fromIntegral num * log (lk bsfs)

-- | Guess at initial model.  If N_e is about 10.000, then the rate 1
-- corresponds to (20000 generation times)^-1.  With a generation time
-- of 25a, that's 500.000 years.  Very roughly, it just needs to be a
-- guess.  The mutation rate per site and generation should be about
-- 10^-6, so the expected number of mutations per block and generation
-- should be about 20000*num_observed*10^-6.
-- (XXX This may be wildly wrong?!)

initial_model :: Int -> Nea.ModelParams Double
initial_model num_observed = Nea.ModelParams
    { chimp_split_rate   = log 12       -- 6Ma
    , neand_split_rate   = log 0.25     -- 125ka
    , human_split_rate   = log 0.05     -- 25ka
    , contact_rate       = log 0.05     -- 25ka
    , mixing_rate        = log 0.0025   -- about 5% of genes in those 25ka
    , admixture_rate     = log 0.05     -- 25ka
    , extinction_rate    = log 0.1      -- 50ka
    , mutation_rate      = log $ fromIntegral num_observed * 0.2 }

