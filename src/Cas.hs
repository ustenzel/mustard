{-# LANGUAGE UndecidableInstances, Rank2Types, MagicHash #-}
-- | A Computer Algebra System
--
-- Symbolic computations with some simplification.  Maybe one day, we
-- can even add the inverse Laplace transform.  We also need lots of
-- derivatives, but we can offload those to the \"ad\" package.

module Cas ( module Cas, module Number ) where

import BasePrelude                  hiding ( (:+), try, many, some, Sum, Void )
import Control.Monad.RWS.Strict     hiding ( Sum )
import Data.Hashable
import Data.MemoTrie
import Data.Text                           ( Text )
import Data.Text.Encoding                  ( decodeUtf8 )
import Data.Text.Lazy.Encoding             ( encodeUtf8 )
import Data.Void
import Number
import Text.Megaparsec              hiding ( State )
import Text.Megaparsec.Char

import qualified Data.ByteString                    as B ( readFile )
import qualified Data.ByteString.Lazy.Char8         as B ( hPut, unlines )
import qualified Data.HashMap.Strict                as M
import qualified Data.Text                          as S
import qualified Data.Text.Lazy                     as T
import qualified Data.Text.Lazy.Builder             as B
import qualified Data.Text.Lazy.Builder.Int         as B
import qualified Data.Text.Lazy.Builder.RealFloat   as B
import qualified Text.Megaparsec.Char.Lexer         as L

instance IsString Expr where
    fromString s | all isDigit s = Var (fromString s)
                 | otherwise     = error $ "dont' want to turn " ++ show s ++ " into a variable"

-- | Expressions.  Instead of subtraction, we use addition and
-- multiplication with (-1), instead of division, we use multiplication
-- and exponentiation with (-1).  Powers are limited to integer powers
-- of arbitrary expressions, but the exponential function takes any
-- argument.  The constant zero should be represented as empty sum; any
-- other constant is a scaled empty product.
--
-- It might be beneficial to enforce a certain normal form; say every
-- expression is a sum, each summand is a product, each factor is a
-- power of an expression.  That might help with simplification, or it
-- might not...

data Expr  = Prod  [Expr] !Number   -- scaled product; scale shall not be zero
           | Var   !Name            -- a named variable
           | Log    Expr            -- natural log
           | Exp    Expr            -- exponential
           | Sum   [Expr]           -- empty sum is zero
           | Pow    Expr  !Int
  deriving ( Show, Eq, Ord, Generic )

instance HasTrie Expr where
  newtype (Expr :->: b) = ExprTrie { unExprTrie :: Reg Expr :->: b }
  trie = trieGeneric ExprTrie
  untrie = untrieGeneric unExprTrie
  enumerate = enumerateGeneric unExprTrie

newtype Name = Name S.Text deriving (Eq, Ord, Hashable)

instance Show Name where show (Name t) = show t
instance IsString Name where fromString = Name . fromString

instance HasTrie Name where
  data (Name :->: b) = NameTrie (String :->: b)
  trie f = NameTrie (trie $ f . Name . S.pack)
  untrie (NameTrie string_trie) (Name t) = untrie string_trie (S.unpack t)
  enumerate (NameTrie string_trie) = map (first (Name . S.pack)) (enumerate string_trie)


-- $ Smart constructors build expressions, simplify them as far as that
-- is cheaply possible, and annotate them with derivatives and prepared
-- substitutions.  All of them fold constants, sumE and prodE order
-- their arguments and try to join \"like\" arguments into simpler
-- expressions.  The simplifications should all be cheap, certainly no
-- worse than O(n log n).

zeroE :: Expr
zeroE = Sum []

oneE :: Expr
oneE = constE 1

constE :: Number -> Expr
constE 0 = zeroE
constE x = Prod [] x


-- We use @exp µ@ instead of plain @µ@, because that avoids the boundary
-- condition at @µ==0@.  (It also turns the uninformative prior @dµ/µ@
-- into the uniform prior @dµ@, which might be easier to work with.)
muE :: Expr
muE = expE $ Var "μ"

scaleE :: Number -> Expr -> Expr
scaleE 1  e           = e
scaleE c (Prod  es d) = prodE (c*d) es
scaleE c (Sum   es)   = sumE $ map (scaleE c) es
scaleE c  e           = Prod [e] c

-- Turning a product into a sum of logs misbehaves; probably due to low
-- precision.  Since it would also be expensive in general, we don't do
-- that.  Pulling a constant out of the log works and rarely opens up an
-- opportunity for further simplification; it looks ugly, though.
logE :: Expr -> Expr
logE (Prod fs c) | c /= 1 = constE (Double . log $ n2d c) + logE (prodE 1 fs)
logE (Exp     x)          = x
logE (Pow   x n)          = scaleE (fromIntegral n) (logE x)
logE  e                   = Log e

expE :: Expr -> Expr
expE (Prod [] (Double f)) = constE . Double . exp $     f
expE (Prod [] (Rat    f)) = constE . Double . exp $ r2d f
expE (Log             x)  = x
expE  e                   = Exp e

infixl 8 `powE`
powE :: Expr -> Int -> Expr
powE _ 0 = oneE
powE e 1 = e
powE (Sum    []) n | n >= 0 = zeroE
powE (Exp     x) n          = expE $ scaleE (fromIntegral n) x
powE (Pow   x m) n          = powE x (m*n)
powE (Prod xs c) n          = prodE (c^^n) (map (`powE` n) xs)
powE  e          n          = Pow e n

-- | Smart sum constructor.  Each summand is normalized to a scaled
-- product.  Summands are sorted, then the scale factors of \"like\"
-- arguments are added.  It's finally passed to 'cfactor', which
-- attempts to factor a constant out.  Statically reciognized zeroes are
-- dropped.
--
-- Logarithms are not treated separately.  It's probably unwise to turn
-- sums of logs into the log of a huge product, because it is certain to
-- overflow.
--
-- Instead of considering only scaled products, we could handle
-- arbitrary products and try to factor and add up the lexically
-- smallest factor.  This requires a different ordering of products.

sumE :: [Expr] -> Expr
sumE = cfactor . combineLike . sort . flattenSums

-- | Turns an expression into a list of scaled expressions to be summed.
-- Unnests one level of 'Sum' and 'Scale' constructors.
flattenSums :: [Expr] -> [( Expr, Number )]
flattenSums = go 1 []
  where
    go _ acc [    ] = acc
    go s acc (n:ns) = case n of
            Prod [Sum xs] c -> go s (go (s*c) acc xs) ns
            Prod [   f  ] c -> go s ((f,s*c) : acc) ns
            Prod    fs    c -> go s ((Prod fs 1,s*c) : acc) ns
            Sum     xs      -> go s (go s acc xs) ns
            _               -> go s ((n,s) : acc) ns

-- | Combines adjacent \"like\" terms by adding their weights.
-- (In practice, weights are either scale factors or exponents.)
combineLike :: (Num a, Eq a) => [( Expr, a )] -> [( Expr, a )]
combineLike ((x,e):(y,f):as) | x == y = combineLike ((x,e+f):as)
combineLike ((_,0)      :as)          = combineLike as
combineLike ((x,e)      :as)          = (x,e) : combineLike as
combineLike [              ]          = []

-- | Sums a list of scaled expressions, but tries to pull out a constant
-- factor.  If the first summand is a product, its scale factor is pulled
-- out.  (This neatly subsumes constants, which have the form
-- @Prod c []@.)  Else we just apply 'Sum'.
cfactor :: [( Expr, Number )] -> Expr
cfactor [     ] = zeroE
cfactor [(e,c)] = scaleE c e
cfactor ((e,c):es) = prodE c [Sum es']
  where
    es'  = e : map (\(x,s) -> scaleE (s/c) x) es

-- | Normalizes a product, attempting to simplify it.  We want to turn
-- @x^i * x^j@ into @x^(i+j)@ and @e^x * e^y@ into @e^(x+y)@, and we
-- must collect constants.  The first must work at least for powers of
-- variables, but arbitrary expressions would be good.
--
-- Possible strategy:  we separately collect constants, exponentials,
-- and everything else.  "Everything else" is always expressed as @x^i@
-- with @i=1@ for any expression that wasn't already of this shape.
-- Then sort, combine using smart constructors, drop ones, reassemble.
prodE :: Number -> [Expr] -> Expr
prodE c [ ] = constE c
prodE 1 [x] = x
prodE c  xs = Prod fs c'
  where
    (c', exps, prods) = flattenProds c xs
    e' = case exps of [ ] -> id
                      [e] -> (expE e :)
                      _   -> (expE (sumE exps) :)
    fs = e' . map (uncurry powE) . combineLike . sort $ prods

-- | Flattens nested products, collects constants and exponentials
-- separately from powers of radicals.
flattenProds :: Number -> [Expr] -> ( Number, [Expr], [( Expr, Int )] )
flattenProds cst0 = go 1 cst0 [] []
  where
    go  _  !cst exps pows [    ] = (cst, exps, pows)
    go !ex !cst exps pows (n:ns) = case n of
            -- early out as soon as we hit a zero
            Sum   [] -> (1,[],[])
            Prod _ 0 -> (1,[],[])

            Pow (Prod xs c) p -> case go (ex*p) (c^^(ex*p) * cst) exps pows xs of
                                    (cst', exps', pows') -> go ex cst' exps' pows' ns
            Pow (Exp     y) p -> go (ex*p) cst (scaleE (fromIntegral (ex*p)) y : exps) pows ns
            Pow  x          p -> go ex cst exps ((x,ex*p) : pows) ns

            Prod xs c -> case go ex (c^^ex * cst) exps pows xs of
                            (cst', exps', pows') -> go ex cst' exps' pows' ns
            Exp x -> go ex cst (scaleE (fromIntegral ex) x : exps) pows ns
            _     -> go ex cst exps ((n,ex) : pows) ns


instance Num Expr where
    fromInteger = constE . fromIntegral
    x + y       = sumE    [x, y]
    x - y       = sumE    [x, scaleE (-1) y]
    x * y       = prodE 1 [x, y]
    negate x    = scaleE (-1) x
    abs x       = x
    signum _    = oneE

instance Fractional Expr where
    fromRational = constE . fromRational
    x / y   = prodE 1 [x, powE y (-1)]
    recip x = powE x (-1)

instance Floating Expr where
    pi  = constE (Double pi)
    log = logE
    exp = expE

    sinh  x = (expE x - expE (-x)) / 2
    cosh  x = (expE x + expE (-x)) / 2
    asinh x = logE $ x + sqrt (powE x 2 + 1)
    acosh x = logE $ x + sqrt (powE x 2 - 1)
    atanh x = logE ((1+x) / (1-x)) / 2

    sin   = error "sin not implemented for Expr"
    cos   = error "cos not implemented for Expr"
    asin  = error "asin not implemented for Expr"
    acos  = error "acos not implemented for Expr"
    atan  = error "atan not implemented for Expr"


type Parser m a = ParsecT Void Text m a

sc :: Monad m => ParsecT Void Text m ()
sc = L.space space1   -- space comsumer
             empty    -- line comment (relevant?)
             empty    -- block comment (relevant?)

lx :: Monad m => ParsecT Void Text m a -> ParsecT Void Text m a
lx = L.lexeme sc

sym :: Monad m => Text -> ParsecT Void Text m Text
sym = L.symbol sc

parse_from_file :: MonadIO m => FilePath -> m Expr
parse_from_file fp = do
    pp <- runParserT parse_expr fp . decodeUtf8 =<< liftIO (B.readFile fp)
    case pp of Left bundle -> liftIO $ hPutStrLn stderr (errorBundlePretty bundle) >> exitFailure
               Right     e -> return e

-- | Parses Mathematica expressions, or at least a useful subset of
-- them.  CSE is achieved on the fly through smart constructors that
-- memoize and recycle everything.
parse_expr :: Monad m => Parser m Expr
parse_expr = esum
  where
    esum  = chainl1 eprod ((+) <$ sym "+" <|> (-) <$ sym "-")
    eprod = chainl1 epow  ((*) <$ sym "*" <|> (/) <$ sym "/")
    epow  = choice [ negate <$ sym "-" <*> epow
                   , try (string "E" >> notFollowedBy alphaNumChar) >> sc >> sym "^" >> parse_term >>= return . expE
                   , parse_term >>= \x -> option x (sym "^" >> lx L.decimal >>= return . powE x) ]

    chainl1 p op = p >>= rest
      where
        rest x = option x $ do f <- op ; y <- p ; rest (f x y)

parse_term :: Monad m => Parser m Expr
parse_term = choice
    [ between (sym "(") (sym ")") parse_expr
    , lx (try L.float) >>= return . constE . Double
    , lx L.decimal >>= return . constE . i2n
    , Var . Name <$> lx param ]
  where
    param :: Monad m => Parser m Text
    param = lx $ try $ do zs <- liftA2 (:) (letterChar <|> greekEscape)
                                           (many $ alphaNumChar <|> greekEscape)
                          guard $ zs /= "E"

                          ys <- option "" $ between (char '[') (char ']') $
                                    fmap ('_' :) $ some $
                                        letterChar <|> char '{' <|> char '}' <|>
                                        greekEscape <|> spaceChar <|> char ','
                          return . fromString . filter (not . isSpace) $ zs ++ ys

    greekEscape :: Monad m => Parser m Char
    greekEscape = do z <- char '\\' >> between (char '[') (char ']') (some letterChar)
                     case z of "Omega"  -> return 'ω'
                               "Lambda" -> return 'Λ'
                               "Mu"     -> return 'µ'
                               _        -> fail $ "it's all greek to me: " ++ z

-- | Simple evaluator.  Useful for debugging, but probably too slow for
-- practical use, especially on deep derivatives.
evalExpr :: M.HashMap Text Double -> Expr -> Double
evalExpr vs = go
  where
    go (Var (Name x)) = vs M.! x
    go (Log        x) = log (go x)
    go (Exp        x) = exp (go x)
    go (Sum       xs) = foldl' (+)    0    $ map go xs
    go (Prod    xs c) = foldl' (*) (n2d c) $ map go xs
    go (Pow      x n) = go x ^^ n


type Eval = RWST (M.HashMap Text Double) () (M.HashMap (StableName Expr) Double) IO

runEval :: Eval a -> M.HashMap Text Double -> a
runEval m args = fst $ unsafePerformIO (evalRWST m args M.empty)

-- | Evaluator for memoized subexpressions.  More useful than
-- 'evalExpr', but probably still too slow for practical use.
evalMemo :: Expr -> Eval Double
evalMemo e = do args <- ask
                mem <- get
                nm <- lift $ makeStableName e
                case M.lookup nm mem of
                    Just  v -> return v
                    Nothing -> do v <- evalP args e
                                  put $! M.insert nm v mem
                                  return v
  where
    evalP as (Var (Name p)) = return $ as M.! p
    evalP  _ (Log        x) = log    <$> evalMemo x
    evalP  _ (Exp        x) = exp    <$> evalMemo x
    evalP  _ (Pow      x n) = (^^ n) <$> evalMemo x
    evalP  _ (Sum       xs) = foldM (\b a -> (b +) <$> evalMemo a)    0    xs
    evalP  _ (Prod    xs c) = foldM (\b a -> (b *) <$> evalMemo a) (n2d c) xs


evalFullMemo :: Functor f => M.HashMap Text Double -> f Expr -> f Double
evalFullMemo vs = fmap (untrie t)
  where
    t :: Expr :->: Double
    t = trie go

    go (Var (Name x)) = vs M.! x
    go (Log        x) = log (untrie t x)
    go (Exp        x) = exp (untrie t x)
    go (Sum       xs) = foldl' (+)    0    $ map (untrie t) xs
    go (Prod    xs c) = foldl' (*) (n2d c) $ map (untrie t) xs
    go (Pow      x n) = untrie t x ^^ n


-- Shows an expression in a readable way.  This is as pretty as
-- possible, it is not proper Haskell syntax.
showE :: Expr -> B.Builder
showE = go (0::Int)
  where
    showC1 1 = id
    showC1 x = (:) (showC x)

    showC (Double x) = B.realFloat x
    showC (Rat    x) | denominator x == 1 = B.decimal (numerator x)
                     | otherwise          = B.decimal (numerator x) <> "/" <> B.decimal (denominator x)


    -- e^x_i becomes Λ_i by convention
    go _ (Exp (Var (Name x)))
        | "x_{" `S.isPrefixOf` x, "}" `S.isSuffixOf` x, S.all isDigit (S.drop 3 (S.init x))
            = "Λ" <> B.fromText (S.tail x)

    -- e^μ is printed as simply μ; likewise, plain μ becomes (log μ)
    go _ (Exp (Var "μ")) = "μ"
    go _ (Var "μ") = "log μ"

    go _ (Var (Name x)) = B.fromText x
    go _ (Log        x) = "log " <> go 10 x
    go _ (Exp        x) = "e^" <> go 10 x
    go _ (Sum       []) = "0"
    go _ (Prod    [] c) = showC c
    go p (Sum       xs) = buildParen (p>6) $ mconcat $ intersperse " + " $ map (go 6) xs
    go p (Prod    xs c) = buildParen (p>7) $ mconcat $ intersperse " * " $ showC1 c $ map (go 7) xs
    go p (Pow      x n) = buildParen (p>8) $ go 8 x <> "^" <> B.decimal n

    buildParen False b = b
    buildParen True  b = "(" <> b <> ")"

-- | Dumps an expression to stdout, assuming the terminal uses UTF8
-- encoding.
printE :: Expr -> IO ()
printE = B.hPut stdout . B.unlines . map encodeUtf8 . flow . B.toLazyTextWith 16368 . showE
  where
    flow s = if T.length l < 120 then [l] else l' : flow r'
      where
        l  = T.take 120 s
        l' = T.dropWhileEnd (not . isSpace) l
        r' = T.drop (T.length l') s
