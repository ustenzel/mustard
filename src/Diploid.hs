{-# LANGUAGE UndecidableInstances #-}
-- | Here we build up the tool set for working with with models in which
-- each present day deme consists of two indisguishable genes.  This
-- corresponds to the common situation, where each deme is really a
-- single diploid individual.

module Diploid
    ( Branch(..)
    , getFinite
    , Deme
    , deme
    , isSingleGene
    , noGene
    , oneGene
    , twoGenes
    , coalesce
    , coalescences
    , ancestralPopulation
    , terminalBranches
    , unmix
    ) where

import BasePrelude
import Data.Finite
import Data.MemoTrie
import GenFunc
import GHC.TypeNats

import qualified Data.Vector.Sized  as V

-- | A branch is defined by a set of genes.  Since genes are labelled by
-- present day demes, a branch is the same as a set of genes.  Because
-- we always have two genes with the same label, it's really a
-- \"two-bounded multiset\" or something like that.  Just like a set can
-- be represented as a number in binary, our \"two-bounded multiset\" is
-- represented as a number in ternary.
--
-- The 'Semigroup' instance computes the branch two given branches merge
-- into by adding their numbers.  This will only work if the two
-- branches are disjoint.  (They always are in our application).

newtype Branch (n :: Nat) = Branch (Finite n) deriving (Eq, Ord, Show)

instance KnownNat n => Semigroup (Branch n) where
    Branch a <> Branch b = Branch $ a+b+1

instance KnownNat n => HasTrie (Branch n) where
  newtype (Branch n :->: b) = BranchTrie (V.Vector n b)
  trie f = BranchTrie (V.generate (f . Branch))
  untrie (BranchTrie v) (Branch i) = V.index v i
  enumerate (BranchTrie v) = zipWith (\i b -> (Branch i, b)) [0..] (V.toList v)


-- | One deme out of n.  Corresponds to a set of genes, which is
-- represented as a list out of convenience.  The genes, in turn, are
-- identified by the branches they are on.
--
-- Given n demes, there are 3^n distinct sets of genes.  Of those, two
-- (the empty set and the full set) are not actually branches.  We leave
-- them out, but this also means that we have to subtract one when
-- computing indices (see 'oneGene').

newtype Deme (n :: Nat) = Deme { unpackDeme :: [Branch (3^n-2)] } deriving (Generic, Show)

deme :: [Branch (3^n-2)] -> Deme n
deme bs | all ((<3) . length) (group bs') = Deme bs'
        | otherwise = error "too many genes in one Deme"
  where
    bs' = sort bs

isSingleGene :: Deme n -> Bool
isSingleGene (Deme [_]) = True
isSingleGene        _   = False


instance Semigroup (Deme n) where Deme a <> Deme b = Deme (a ++ b)
instance Monoid    (Deme n) where mempty = Deme [] ; mappend = (<>)

instance KnownNat (3^n-2) => HasTrie (Deme n) where
  newtype (Deme n :->: b) = DemeTrie { unDemeTrie :: Reg (Deme n) :->: b }
  trie = trieGeneric DemeTrie
  untrie = untrieGeneric unDemeTrie
  enumerate = enumerateGeneric unDemeTrie


infixl 5 @+, @-

-- | Adds a gene on a given branch to a deme.
(@+) :: Deme n -> Branch (3^n-2) -> Deme n
Deme gs @+ b = Deme $ b : gs

-- | Removes a gene on a given branch from a deme.
(@-) :: Deme n -> Branch (3^n-2) -> Deme n
Deme gs @- b = Deme $ delete b gs

-- | Given a deme with multiple genes, returns the deme with a single
-- gene on their ancestral branch.
coalesce :: KnownNat (3^n-2) => Deme n -> Deme n
coalesce (Deme []) = Deme []
coalesce (Deme gs) = Deme [ foldr1 (<>) gs ]


noGene :: Deme n
noGene = Deme []

-- | Creates a deme consisting of one gene, i.e. a single haploid
-- individual.  If a gene is in deme \(i\), it is on branch \(3^i\).
--
-- Note that 'Branch 0', which is the nonsensical concept of a branch
-- separating no one, doesn't have a dummy parameter, and is never
-- represented.  Therefore, we subtract one from the index.

oneGene :: KnownNat (3^n-2) => Finite n -> Deme n
oneGene i = Deme [ Branch $ 3 ^ getFinite i - 1 ]
    -- (3^i) diverges if n == 2, so 'getFinite' to avoid the problem

-- | Creates a deme consisting of two genes, i.e. a diploid individual.
twoGenes :: KnownNat (3^n-2) => Finite n -> Deme n
twoGenes i = oneGene i <> oneGene i



-- | Terminal branches contribute their dummy parameter to the total
-- rate, but no generating function of their own.  This is conveniently
-- expressed as contributing nothing at the rate given by a dummy
-- parameter.
terminalBranches :: (KnownNat n, KnownNat m, m ~ (3^n-2), Num t)
                 => Deme n -> GFBuild pop mod m t
terminalBranches (Deme gs) _ omegas _ =
    foldMap (\(Branch b) -> WS 0 (V.index omegas b)) gs


-- | Coalescence is possible between any pair of genes in a 'Deme'.
coalescences :: (HasTrie pop, KnownNat n, KnownNat m, m ~ (3^n-2), Num t)
             => (Deme n -> pop) -> Deme n -> GFBuild pop mod m t
coalescences mkpop gs =
    mconcat [ 1 .*. mkpop (gs @- a @- b @+ (a <> b))
            | a : gs' <- tails $ unpackDeme gs
            , b <- gs' ]


-- | This is the base case for generating functions.  It should be
-- applied whenever there is only one gene left.
--
-- [Note]  There is only one \"tree\" with a single node, and it doesn't
-- have branches.  Its probability is one.  There are no timing
-- parameters, hence there is nothing we could apply a Laplace Transform
-- to, hence the GF must also be the constant one.  Tenuous as this
-- reasoning sounds, it also matches Konrad's papers.
ancestralPopulation :: Num t => GFBuild pop mod n t
ancestralPopulation _ _ _ = WS 1 1


-- | Implements gene flow in reverse.  Mixing happens at rate
-- @mixrate@ (i.e., each gene is expected to transfer once every
-- @1/mixrate@ generations), and @mkpop@ reconstructs the ancestral
-- population.
unmix :: (HasTrie pop, KnownNat n, KnownNat m, m ~ (3^n-2), Num t)
      => t -> (Deme n -> Deme n -> pop) -> Deme n -> GFBuild pop mod m t
unmix mixrate mkpop gs =
    foldMap (\g -> mixrate .*. mkpop (gs @- g) (mempty @+ g)) (unpackDeme gs)
