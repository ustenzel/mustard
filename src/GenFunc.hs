-- | Here we implement the computation of likelihoods under the
-- coalescent theory, affectionally knows as \"The Lohse Method\".  Our
-- working hypothesis is that Haskell with Automatic Differentiation
-- (AD) can scale to bigger and better models than Mathematica with
-- symbolic operations.  The basic method was described in [A general
-- method for calculating likelihoods under the coalescent process]
-- (https://www.genetics.org/content/189/3/977.short), and we
-- first recap that paper with some additional detail filled in.  We
-- then show how AD can replace symbolic computation, and finally
-- construct a Generating Function for a toy problem.
--
-- = Likelihoods Under The Coalescent
--
-- Let's assumed we have the sequence of a small number homologous genes
-- from different demes.  [I will not call them \"populations\" under
-- any circumstances.  That term is too ill-defined to deserve any use.]
-- These sequences align and display some number of mutations.  Each
-- mutation is common to some subset of genes.  We seek to calculate the
-- likelihood of this particular set of mutations occuring under some
-- model of the history of these demes.
--
-- According to Coalescense Theory, these genes all derive from a common
-- ancestor.  Their history can be described by a binary tree, called
-- the phylogenetic tree.  Mutations are common to a subset of genes,
-- because they happened on their common branch of the phylogenetic
-- tree.  We cannot know the tree, instead, we have to consider a
-- distribution of trees, from which the genes collectively sample one
-- tree.
--
-- [Note] The language of coalescence theory is somewhat backwards, as
-- if time was reversed.  We will consistently talk about time flowing
-- forward, so populations split and Neandertards die out.  Genes still
-- coalesce, because that's the established terminology.  When we trace
-- back time, the \"first\" event we have to consider in code is the
-- \"most recent\" one.
--
-- Trees vary in both their shape and their measurements.  Instead of
-- trying to represent trees, we will only consider the distribution of
-- branch lengths.  A branch is defined as the set of genes below it.  A
-- particular tree may not contain a certain branch, and in that case,
-- we set that branch length to zero.  So we should compute the joint
-- probability density function (PDF) of the branch lengths of a
-- collection of trees.
--
-- To construct the PDF, we consider what was the most recent event in
-- the history of our set of genes, when it happened, and how the set of
-- genes looked before.  For concreteness, let's say we have three genes
-- from the same deme.  The only thing that can happen is coalescence,
-- between any two genes.  The time to coalescence is distributed
-- exponentially.  Let's say genes \"a\" and \"b\" coalesce, then the
-- lengths of the branches \"a\" and \"b\" are the same and, for
-- sufficiently large values of \(N_e\) follow the PDF
--
-- \[ p(t_a) = \left. \frac{1}{2N_e} e^{-\frac{t_a}{2N_e}} \right|_{t_a \geq 0} \]
--
-- [Note]  I've seen the term \(t_a-1\) used in the numerator of the
-- exponent instead.  I'll write that off as \"biologists irrationally
-- starting to count from one instead of zero\".
--
-- Earlier than that, there were two genes, and they again coalesce
-- after time \(t_{ab}\), which follows the same PDF.  Now consider the
-- branch length of the third gene \"c\".  Since it only participated in
-- the earlier coalescence event, the branch length is the sum of
-- \(t_a\) and \(t_{ab}\), which gives the PDF:
--
-- \[ p(t_c) = \int_0^\infty p(t_{ab}=t_c-t_a) p(t_a) dt_a \]
--
-- The integral looks innocent now, but it becomes ugly quickly with
-- more genes and more complicated scenarios.  It's so ugly, Biologists
-- decided, solving it is not an option, and simply gave it the name
-- \"Felsenstein Integral\".  We will not solve it, either.
--
-- = Recap Of Lohse et.al.
--
-- == Laplace Transform To The Rescue
--
-- For a function \(f(t)\), its Laplace transform
-- \(\mathcal{L}\{f\}(\omega)\) is defined by
-- \[ \mathcal{L}\{f\}(\omega) = \mathbb{E}\left[ e^{-\omega t} \right]
--                             = \int_0^\infty f(t) e^{-\omega t} dt \]
--
-- The Laplace transform of the PDF of a random variable is
-- conventionally called the Laplace tranform of the random variable.
-- Since it is closely related to the [Moment Generating
-- Function](https://en.wikipedia.org/wiki/Moment_generating_function),
-- we will call it the Generating Function (GF).
-- Whereas \(f\) is a function of a time-valued parameter,
-- \(\mathcal{L}\{f\}\) takes a parameter that is the reciprocal of
-- time, or a rate.  This \(\omega\) is called a /dummy parameter/.
--
-- The Laplace transform has nice algebraic properties which fit
-- coalescence theory particularly well:
--
-- * It's linear, that is, GFs can be scaled and added instead of
-- scaling and adding the functions they derive from.
-- * The GF of an exponential function is particularly simple:
--
-- \[ \mathcal{L}\left\{ p(t_a) \right\}
--              = \mathcal{L}\left\{ \frac{1}{2N_e} e^{-\frac{t_a}{2N_e}} \right\}
--              = \frac{1}{2N_e} \frac{1}{\omega_a + \frac{1}{2N_e}}
--              = \frac{1}{2N_e \omega_a + 1} \]
--
-- XXX  Check if that mysterious (+1) term is in the Lohse papers and
-- why or why not.
--
-- * The GF of a convolution integral is the product of two GFs.  The
-- ugly Felsenstein Integral happens to be a convolution integral.
--
-- There are more rules, which can be found in a suitable math textbook
-- or [any encyclopedia](https://en.wikipedia.org/wiki/Laplace_Transform).
-- GFs are constructed using algebraic rules instead of evaluating an
-- improper integral.   In the motivating example, we take the GF of
-- \(p(t_a)\), which is a constant, and multiply it with the GF of
-- \(p(t_{ab})\), then add similar terms for the cases where another
-- pair of genes coalesces most recently.  This is easy to code and
-- yields a term composed entirely of basic operations.
--
-- The price we pay for this convenience is that instead of a PDF, we
-- get its GF.  Inverting the Laplace transform is out of the question,
-- because not only is it difficult, it would just give us the nested
-- Felsenstein Integrals back.
--
-- == Extracting Likelihoods From The GF
--
-- === Infinite Sites Approximation
--
-- We want to extract likelihoods directly from the GF.  Consider a
-- single branch we observed \(k\) mutations on.  Assume a mutation rate
-- \(\mu\), assume infinite sites, i.e. mutations cannot undo mutations.
-- Given the branch length \(t\), the number of mutations follows a
-- Poisson distribution:
--
-- \[ P(k) = \frac{(\mu t)^k}{k!} e^{-\mu t} \]
--
-- Since we can't know \(t\), we shall integrate it out:
--
-- \[ P(k) = \int_0^\infty \frac{(\mu t)^k}{k!} e^{-\mu t} p(t) dt \]
--
-- This isn't the Laplace transform of anything, but if it was, we could
-- hope to transform it into an expression in terms of the GF.  Note
-- that \( e^{-\omega t} = 1 \) for any \(t\) if we set \(\omega = 0\).
-- So we multiply the last equation by 1, and now it is the Laplace
-- transform of something, evaluated at zero:
--
-- \[ P(k) = \left. \int_0^\infty \frac{(\mu t)^k}{k!} e^{-\mu t} p(t) e^{-\omega t} dt \right|_{\omega=0} \]
--
-- by definition of Laplace Transform
--
-- \[ P(k) = \left. \mathcal{L} \left\{ \frac{(\mu t)^k}{k!} e^{-\mu t} p(t) \right\} \right|_{\omega=0} \]
--
-- by linearity
--
-- \[ P(k) = \left. \frac{\mu^k}{k!} \mathcal{L} \left\{ t^k e^{-\mu t} p(t) \right\} \right|_{\omega=0} \]
--
-- by frequency domain derivative
--
-- \[ P(k) = \left. \frac{(-\mu)^k}{k!} \frac{\partial^k}{\partial\omega^k}
--              \mathcal{L} \left\{ e^{-\mu t} p(t) \right\} \right|_{\omega=0} \]
--
-- by frequency shift by \(-\mu\)
--
-- \[ P(k) = \left. \frac{(-\mu)^k}{k!} \frac{\partial^k}{\partial\omega^k}
--              \mathcal{L} \left\{ p(t) \right\} \right|_{\omega=\mu} \]
--
-- by definition of GF
--
-- \[ P(k) = \frac{(-\mu)^k}{k!} \frac{\partial^k}{\partial\omega^k} \mbox{GF}(\mu) \]
--
-- The derivation generalizes in the obvious way:  to compute the
-- likelihood of \(k\) mutations on branch \(i\), derive partially \(k\)
-- times with respect to \(\omega_i\), then set \(\omega_i = \mu\), and
-- multiply by \((-\mu)^k/k!\).  To ignore the number of mutations on
-- branch \(i\), set \(\omega_i = 0\).  Do either for each branch, and
-- what's left is a function of \(\mu\) and whatever other parameters
-- went into the GF, but the dummy parameters \(\omega_i\) have
-- disappeared.
--
-- It should be noted that certain combinations of mutations are
-- impossible under the infinite sites approximation.  If we ever
-- attempt to calculate their likelihood, we get zero, and subsequent
-- calculations will break down.  We will deal with this later.  (XXX
-- Where?)
--
-- === Finite Sites
--
-- Dropping the infinite sites approximation turns the Poisson
-- distribution into a hypergeometric distribution.  Following the same
-- derivation as above, again yields an expression in terms of the GF.
-- Unfortunately, for a sequence length of \(n\) and \(k\) rate
-- parameters under consideration, it requires \(n^k\) evaluations of
-- the GF to calculate a single likelihood.  Since this means millions
-- to billions of evaluations even for short genes of \(n \approx 100\)
-- and tiny models with data radically simplified to \(k \approx 4\),
-- this is clearly impossible without applying a different
-- approximation.
--
-- Given that we got plenty of work to do anyway, finite sites will not
-- be considered any further.
--
-- == The General Recursion
--
-- The GF is constructed recursively.  We look at a collection of demes
-- which have some historic relationship.  Now consider all events that
-- could possibly have happened most recently.  Each event transforms
-- the population.  The GF of this population is the sum of the GFs of
-- the possible populations before the transformation, each multiplied
-- by the rate of that event, divided by the total rate of events.  The
-- total rate of events is the sum of the rates of the events just
-- considered, plus the rate parameters of the branches the presently
-- existing genes are on.  The base case is a single deme with a single
-- gene, and the generating function is simply one.
--
-- The reason for this simple form of the GF is that the timings of all
-- events follow exponential distributions.  Therefore, the time of the
-- most recent event is also exponentially distributed, with a rate
-- that's the sum of the individual rates.  The Laplace transform of
-- that is a constant, and convolution with the GFs for the previous
-- states corresponds to multiplication, so we get a weighted sum.
--
-- [Note]  I think, the weird definition of the total rate is to account
-- for unseen coalescences within demes.  I have no idea why this is
-- done, though. XXX
--
-- [Note]  In practice, multiple sequences of events lead to the same
-- state of the population.  Some sort of dynamic programming or
-- memoization will be necessary to keep runtime under control.
--
-- === Population Splits
--
-- Suppose the population consists of two demes with their own set of
-- genes.  Three kinds of most recent event need to be considered:
-- coalescence within the first deme, coalescence within the second
-- deme, and splitting of the demes.  (Coalescence /between/ demes
-- is not possible unless the demes haven't split yet, and that's
-- obviously dealt with in the recursively constructed GFs.)  The demes
-- split at some time \(T\).
--
-- According to Lohse et.al., introducing \(T\) directly is awkward.
-- (This is presumably because we weight events with their rate, and
-- something that happens at a discrete time doesn't have a simple
-- rate.)  But we can pretend that demes split at a /rate/ \(\Lambda\),
-- with the split time distributed exponentially.  The GF has a simple
-- form again, but instead of a time-valued parameter, we're now stuck
-- with an artificial rate-valued paramter \(\Lambda\), which will
-- appear in our likelihood functions.  There are two ways to deal with
-- that:
--
-- * Invert the Laplace transform with respect to \(\Lambda\).  This is
-- between hard and impossible in general, but our GFs supposedly follow
-- a simple structure and the inverse Laplace transform is relatively
-- easy.  It does appear to require symbolic computation, though.
--
-- * Suck it up.  Since all times are artificial model parameters anyway
-- (they depend on assumed generation times, are scaled by certainly
-- wrong effective population sizes, it wasn't a single discrete event anyway,
-- etc.), which have no meaning except when compared to each other, we
-- might as well work with the rate parameters instead.  They are as
-- artificial, and as easy to interpret relative to each other.
--
-- We'll go with the second approach, mostly to avoid the complications
-- of symbolic computation.
--
-- === Unidirectional Gene Flow
--
-- The event is easily modelled:  take a gene from one deme and add
-- it to another.  The recursion is still well defined.  As with
-- splits, the cleanest way to to it is to assume a constant rate of
-- flow.
--
-- [Note]  If I'm not mistaken, gene flow between the demes that split
-- most recently is the same as "Isolation with (Unidirectional)
-- Migration".  Good enough.
--
-- === Bidirectional Gene Flow
--
-- If genes can flow back and forth, we can (rarely, and theoretically)
-- get back to the same state of the population.  This causes the
-- recursion to diverge.  However, our recursion produces only linear
-- equations, so even if they end up being self-referential, we get a
-- linear system of equations, which is easy to solve.
--
-- Lohse et.al. cautions, that solving equation systems symbolically is
-- awkward, yields horrible expressions, and there is no obvious way to
-- compute derivates of the solutions.  We will have to say more about
-- that later.
--
-- At any rate, bidirectional gene flow is decidedly a complication and
-- best avoided.  We will avoid it for the time being.
--
--
-- = Automatic Differentiation (AD)
--
-- Lohse et.al. computed symbolically in Mathematica.  We will instead
-- code our GF in Haskell and apply AD to get derivatives.  This has a
-- few advantages:
--
-- * Partial derivatives are large expressions, but many subexpressions
-- repeat.  (The size of the /k/th derivative can be exponential in /k/, but
-- the number of distinct subexpressions is linear in /k/.)  In AD, these
-- subexpressions are evaluated, and it is easy to share the results.
-- We should be able to handle bigger problems and higher numbers of
-- mutations without running out of memory.
--
-- * Any program can be differentiated.  We could implement any
-- algorithm to solve equational systems (Gauss elimination,
-- LU-decomposition, ...), and our results would still have easily
-- accessible partial derivatives.  The algorithm doesn't even need to
-- know that it operating on expressions that still need to be
-- differentiated, it only sees ordinary numbers.  This might take the
-- awkwardness out of bidirectional gene flow.
--
-- And disadvantages:
--
-- * There is no obvious way to invert the Laplace transform.
--
-- [Note]  I wonder if a hybrid is possible where we construct a linear
-- system of equations for the GF, inverse transform each (with respect
-- to Λ, where ever split times are involved), then solve the resulting
-- system.  Laplace transform is linear, so this should work?)


module GenFunc
    ( module GenFunc
    , module Control.Comonad.Cofree
    )
  where

import BasePrelude
import Control.Comonad.Cofree ( Cofree(..) )
import Data.Finite            ( Finite )
import Data.Hashable          ( Hashable(..) )
import Data.MemoTrie
import GHC.TypeNats

import qualified Codec.Serialise                     as Cborg
import qualified Data.Vector.Sized                   as V
import qualified Data.Vector.Unboxed.Sized           as U
import qualified Numeric.AD.Mode.Sparse              as AD


-- | The BSFS is a mapping from branches to the number of mutations on
-- them.  Since 'Branch'es are mapped to integers, we can just use
-- a 'Vector'.  @n@ is the number of distinct 'Branch'es.

newtype BSFS (n :: Nat) = BSFS { fromBSFS :: U.Vector n Word }
    deriving (Eq, Ord, Show, Hashable)

instance KnownNat n => Cborg.Serialise (BSFS n) where
    encode (BSFS v) = Cborg.encode (U.fromSized v)
    decode          = Cborg.decode >>= maybe (fail "length mismatch") (pure . BSFS) . U.toSized


-- | A weighted sum of components.  Tracks the sum itself, and the sum
-- of weights.  This is used to make expressing the recursion in
-- Generating Functions easier.
data WeightedSum a = WS { _ws_total :: !a, _ws_weight :: !a }

instance Num a => Semigroup (WeightedSum a) where
    WS a b <> WS c d = WS (a+c) (b+d)

instance Num t => Monoid (WeightedSum t) where
    mempty = WS 0 0
    mappend = (<>)

getWeightedSum :: (Fractional t, Eq t) => WeightedSum t -> t
getWeightedSum (WS s w)
    | w == 0            =  error "[getWeightedSum] div by zero.  Did you forget the base case for a generating function?"
    | otherwise         =  s / w


-- | A generating function takes a set of parameters appropriate for the
-- model, and a vector of dummy parameters corresponding to branches.
-- It needs to be polymorphic over the numeric type to support AD.  For
-- a concrete example, and how to construct it, see "Neandertards".  The
-- type parameters are:
-- [f] the structure of model parameters
-- [n] the number of distinct 'Branch'es
-- [a] the number type, polymorphic for AD to work

type GF f n a = f a -> V.Vector n a -> a

-- | Short hand for the building blocks of a GF.  Type parameters:
-- [p] the dtata structure describing a population
-- [f] model parameters suitable for p
-- [n] number of distinct branches
-- [a] the number type, polymorphic for AD to work

type GFBuild p f (n :: Nat) a = f a -> V.Vector n a -> p :->: a -> WeightedSum a

-- | An event happening at a certain rate.  Contributes the memoised
-- value of the generation function at the specified rate, thereby
-- avoiding repeated recursive evaluation.
(.*.) :: (HasTrie pop, Num t) => t -> pop -> GFBuild pop mod n t
r .*. e = \_ _ tt -> WS (r * untrie tt e) r
{-# INLINE (.*.) #-}
infix 7 .*.


-- | A memoized GF, wraps one that receives the memo trie as argument.
--
-- Note that when the memo trie is built, the GF is fully applied.  This
-- makes it a trie of values, ensuring that expensive calculations are
-- performed only once.  A trie of functions, which is approximately
-- what Konrad builds in Mathematica, would be less useful, because we
-- would still have to evaluate them repeatedly.

generatingFunction :: (Eq t, Floating t, HasTrie pop)
                   => (pop -> GFBuild pop mod n t) -> pop -> GF mod n t
generatingFunction gf pop0 mp omegas = untrie tt pop0
  where
    tt = trie $ \pop -> getWeightedSum $ gf pop mp omegas tt


-- | The structure obtained when a generating function is applied to
-- model parameters, but not the bSFS.  It contains an infinite tower of
-- partial derivatives.

type GenDerivatives (n :: Nat) a = Cofree (V.Vector n) a

-- | Given a generating function, the necessary model parameters, a
-- Block Site Frequency Spectrum (BSFS), and a mutation rate, we seek to
-- compute the likelihood.  The 'BSFS' has to count mutations on
-- branches; correspondingly, the generating function takes dummy
-- parameters for branches.
--
-- We need to derive multiple times with respect to the dummy
-- parameters, then substitute them with the mutation rate.  This sounds
-- as if it needs symbolic calculation, but can in fact be done with AD.
-- We run the GF in sparse tower mode, which gives us all the partial
-- derivatives.  To run AD, a value needs to be passed in for each
-- parameter.  This is very convenient, we can simply pass the mutation
-- rate.  In a sense, we have to first substitute the mutation rate for
-- the dummy parameters, then compute derivatives, while a computer
-- algebra system has to it in the opposite order.
--
-- It's enough to treat only the dummy parameters as variables, while
-- the model parameters are treated as constants.  Later, they, too,
-- will be variable, and we will compute a gradient with respect to
-- them.  Conveniently, this is all hidden in the polymorphic type 'a'.
-- This way, we don't compute expensive towers with respect to model
-- parameters, nor do we compute useless gradients with respect to dummy
-- parameters.
--
-- [Note]  For now, all mutations are deemed interesting, so we always
-- pass the mutation rate.  We might reconsider if we have to pass zeros
-- in some context, to compute, say, the likelihood that some number of
-- mutations is exceeded.
--
-- [Note]  The way this is written, the tower of derivatives can in
-- principle be memoized and then used for computing the likelihood of
-- multiple 'BSFS'es.  I wonder if that's what GHC ends up doing?

applyGenFunc :: forall f n a . (Functor f, KnownNat n, Fractional a)
             => (forall s . GF f n (AD.AD s (AD.Sparse a)))   -- ^ generating function
             -> f a                                           -- ^ model parameters
             -> (f a -> a)                                    -- ^ get mutation rate from model parameters
             -> GenDerivatives n a
applyGenFunc gf args mu = AD.grads (gf (fmap AD.auto args)) (V.replicate (mu args))

likelihood :: forall n a . (KnownNat n, Fractional a)
           => GenDerivatives n a -> a -> BSFS n -> a
likelihood partials mu = finish . U.ifoldl' go1 (0, (1, partials)) . fromBSFS
  where
    finish :: (Word, (Word, Cofree (V.Vector n) a)) -> a
    finish (ex, (den, val :< _)) = bool id negate (odd ex) $ val * mu ^ ex / fromIntegral den

    -- deal with element i of the BSFS, which has value k:  take the
    -- i'th derivative k times, increase the exponent by k, multiply the
    -- denominator with (k!)
    go1 :: (Word, (Word, Cofree (V.Vector n) a)) -> Finite n -> Word -> (Word, (Word, Cofree (V.Vector n) a))
    go1 (!ex, (!den, fs)) i k = (ex + k, go2 den fs i k)

    go2 :: Word -> Cofree (V.Vector n) a -> Finite n -> Word -> (Word, Cofree (V.Vector n) a)
    go2 !den fs           _ 0 = (den, fs)
    go2 !den (_ :< grads) i k = go2 (den * k) (V.index grads i) i (k-1)


-- | Just like 'likelihood', but folds the bSFS.  Because we cannot know
-- which allele was ancestral, we need to normalize away the difference
-- between ancestral and derived allele.  This is done by using the same
-- branch label, whether we talk about the set of demes \"below\" or
-- \"above\" a branch.
--
-- We don't need any fancy arithmetic to do this.  The numbering of our
-- branches is such that reversing the vector of dummy variables
-- corresponds to exchanging the role of ancestral and derived allele.
-- So all we have to do is duplicate the dummy variables appropriately.
--
-- Note the type level arithmetic:  if the folded bSFS has (n+1)
-- distinct branches, one of which is its own mirror image, the full
-- bSFS has (2n+1) distinct branches.

applyGenFuncFolded :: forall f n a . (Functor f, KnownNat (n+1), Fractional a)
                  => (forall s . GF f (n+1+n) (AD.AD s (AD.Sparse a)))   -- ^ generating function
                  -> f a                                                 -- ^ model parameters
                  -> (f a -> a)                                          -- ^ mutation rate inside model parameters
                  -> GenDerivatives (n+1) a
applyGenFuncFolded gf =
    applyGenFunc (\mp omegas -> gf mp (omegas V.++ V.reverse (V.init omegas)))

