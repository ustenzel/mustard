-- module Lizard ( Population(..), ModelParams(..), memoGeneratingFunction, today ) where
module Lizard where

import BasePrelude hiding ( Solo )
import Cas
import Data.MemoTrie
import Diploid
import GenFunc

import qualified Data.Vector.Sized  as V

-- The goal looks like this:  The are two demes (A,B) of two
-- genes each. A = Viridis, B = Bilineata.
--
-- All demes have two genes, because they are diploid individuals. The
-- two genes are indistinguishable, because we assume unphased
-- genotypes.  Obviously, the idea is to estimate the time of the split
-- and the timing and magnitude of later gene flow.

data Population = Solo  (Deme 2)                    -- ^ before the viridis split
                | Pure  (Deme 2) (Deme 2)           -- ^ after the split
                | Happy (Deme 2) (Deme 2)           -- ^ in contact with Billineata
                | Mixed (Deme 2) (Deme 2)           -- ^ isolated after contact
  deriving (Generic, Show)

instance HasTrie Population where
  newtype (Population :->: b) = PopulationTrie { unPopulationTrie :: Reg Population :->: b }
  trie = trieGeneric PopulationTrie
  untrie = untrieGeneric unPopulationTrie
  enumerate = enumerateGeneric unPopulationTrie


-- | The population today: two Viridis genes in the Viridis deme (0), and
-- two Bilineata genes in the Bilineata deme (1).
today :: Population
today = Mixed (twoGenes 0) (twoGenes 1)


-- | Parameter set suitable for an 'Population'.  The rate parameters
-- introduced here are all timing parameters for discrete events.  Note
-- that time is rescaled so that the rate of coalescence is always one.
-- (This assumes the same effective population size on all branches.)

data ModelParams a = ModelParams
    { virSplitRate     :: !a      -- ^ measured since the admixture
    , contactRate      :: !a      -- ^ duration of the admixture event
    , mixingRate       :: !a      -- ^ rate of genes migrating
    , admixtureRate    :: !a      -- ^ rate of admixture events
    , mutationRate     :: !a }    -- ^ uniform, scaled mutation rate
  deriving (Functor, Foldable, Traversable, Show)


-- | Recursively constructs the GF.  See
-- 'Neandertard.memoGeneratingFunction' for a long a boring explanation.
--
-- The 'Mixed' case is most interesting:  the way it's written,
-- \"viridis\" can receive genes from \"bilineata\", but not the other
-- way around, much less in both directions.  Gene flow in the other
-- direction would be an alternative model.
--
-- Gene flow in both directions
-- requires a different implementation strategy, because different
-- parts of the GF would be mutually recursive, instead of evaluationg a
-- single expression, we'd have to solve a system of linear equations.

memoGeneratingFunction :: Floating t => Population -> GFBuild Population ModelParams 7 t
memoGeneratingFunction pop ModelParams{..}  =  gf pop ModelParams{..}
  where
    gf (Solo vir) | isSingleGene vir = ancestralPopulation
                  | otherwise        = terminalBranches vir <> coalescences Solo vir

    gf (Pure    vir bill) = foldMap terminalBranches [vir, bill] <>
                            coalescences (`Pure` bill) vir <>
                            coalescences (vir `Pure`) bill <>
                            exp virSplitRate .*. Solo (vir <> bill)

    gf (Happy   vir bill) = foldMap terminalBranches [vir, bill] <>
                            coalescences (`Pure` bill) vir <>
                            coalescences (vir `Pure`) bill <>
                            unmix (exp mixingRate) (\vir' bill' -> Happy vir' (bill <> bill')) vir <>
                            exp contactRate .*. Pure vir bill

    gf (Mixed   vir bill) = foldMap terminalBranches [vir, bill] <>
                            coalescences (`Pure` bill) vir <>
                            coalescences (vir `Pure`) bill <>
                            exp admixtureRate .*. Happy vir bill


-- ad-hoc testing below
my_model :: ModelParams Expr
my_model = ModelParams
    { virSplitRate  = Var "x_{1}"
    , contactRate   = Var "x_{2}"
    , mixingRate    = Var "x_{3}"
    , admixtureRate = Var "x_{4}"
    , mutationRate  = Var "μ"     }


test :: Population -> IO ()
test pp = printE $ generatingFunction memoGeneratingFunction pp my_model omega
  where
    omega = V.generate $ \i -> om $ min i (maxBound - i)
    om  i = Var . Name $ "ω_{" <> fromString (show (getFinite i)) <> "}"
