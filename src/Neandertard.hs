-- module Neandertard ( Population(..), ModelParams(..), memoGeneratingFunction, today ) where
module Neandertard where

import BasePrelude hiding ( Solo )
import Cas
import Data.MemoTrie
import Diploid
import GenFunc

import qualified Data.Vector.Sized  as V

-- ^ The goal looks like this:  The are four demes (A,B,C,D) of one
-- diploid individual each.  A and B split most recently, after they
-- split from C, and all that after they split from D.  After the most
-- recent split, C admixes into A, and after that, C becomes extinct.
-- Put another way, A = European, B = African, C = Neandertard,
-- D = Chimpanzee reference genome.
--
-- All demes have two genes, because we assume diploid individuals.  The
-- two genes are indistinguishable, because we assume unphased
-- genotypes.  It's possible that real data has only one gene in a deme
-- (e.g. if deme D is indeed just the reference genome), but that is
-- siply a smaller 'Population', which we can easily represent.
--
-- Obviously, the idea is to estimate the magnitude and time of the gene
-- flow, and possibly to date the Neandertard fossil.  Straight forward
-- modifications might allow for the estimation of population sizes.


-- | State of the population.  Our population goes through discrete
-- stages, and at each stage, it is represented as multiple collections
-- of genes called 'Deme's.
--
-- Note that in state 'Sad', the Neandertard is dead.  We still track
-- its 'Deme', it just doesn't participate in coalescence.  This results
-- in its branch length being shorter, and the fossil not accumulating
-- differences while in the ground.
--
-- This model contains interesting sub-models, however, it does not
-- contain a sub-model without admixture.  The reason is that without
-- the admixture event, the human deme can split after the Neandertard
-- is already extinct.  The corresponding \"sad trio\" state is missing.
--
-- Also note that the encoding may seem needlessly complicated and
-- repetitive.  I consider this worthwhile, because it makes invalid
-- states impossible to represent, and because it should allow for a
-- more compact memo trie.  (It also turns out that defining the
-- 'HasTrie' instance is trivial, thanks to generics.)

data Population = Solo  (Deme 4)                                -- ^ before the hominini split
                | Duo   (Deme 4) (Deme 4)                       -- ^ after splitting from chimps
                | Trio  (Deme 4) (Deme 4) (Deme 4)              -- ^ after splitting from Neandertards
                | Pure  (Deme 4) (Deme 4) (Deme 4) (Deme 4)     -- ^ after the humans split
                | Happy (Deme 4) (Deme 4) (Deme 4) (Deme 4)     -- ^ in contact with Neandertards
                | Mixed (Deme 4) (Deme 4) (Deme 4) (Deme 4)     -- ^ isolated after contact
                | Sad   (Deme 4) (Deme 4) (Deme 4) (Deme 4)     -- ^ after the Neandertard dies out
  deriving (Generic, Show)

instance HasTrie Population where
  newtype (Population :->: b) = PopulationTrie { unPopulationTrie :: Reg Population :->: b }
  trie = trieGeneric PopulationTrie
  untrie = untrieGeneric unPopulationTrie
  enumerate = enumerateGeneric unPopulationTrie


-- | The population today: two european genes in the european deme (0),
-- two african genes in the african deme (1), two neandertard genes in
-- the neandertard deme (2), and one chimp gene in the chimp deme (3).
today :: Population
today = Sad (twoGenes 0) (twoGenes 1) (twoGenes 2) (oneGene 3)

-- | Parameter set suitable for a 'Population'.  The rate parameters
-- introduced here are all timing parameters for discrete events.  Note
-- that time is rescaled so that the rate of coalescence is always one.
-- (This assumes the same effective population size on all branches.  It
-- also matches the conventions in Konrad's papers.)
--
-- All parameters are actually log-transformed.  Without this, the rates
-- would be constrained to be positive, but constrained optimization is
-- awkward at best.  After the log-transformation, no constraints
-- exist, and the log also has generally better behaved asymptotics.
-- We should be able to get reasonable confidence intervals from
-- Maxmimum Likelihood estimators.
--
-- We model gene flow as a continuous process during a finite time
-- window.  'admixture_rate' determines the time since contact,
-- 'contact_rate' determines the duration of the mixing event, and
-- 'mixing_rate' determines how frequently genes migrate.  The expected
-- fraction of admixed genes in the mixed population is then
-- @mixing_rate / contact_rate@.

data ModelParams a = ModelParams
    { chimp_split_rate   :: !a      -- ^ measured on the homo branch
    , neand_split_rate   :: !a      -- ^ measured on the human branch
    , human_split_rate   :: !a      -- ^ measured since the admixture
    , contact_rate       :: !a      -- ^ duration of the admixture event
    , mixing_rate        :: !a      -- ^ rate of genes migrating
    , admixture_rate     :: !a      -- ^ rate of admixture events, measured since extinction
    , extinction_rate    :: !a      -- ^ rate of Neandertards extinction
    , mutation_rate      :: !a }    -- ^ uniform, scaled mutation rate
  deriving (Functor, Foldable, Traversable, Show)


-- | Recursively constructs the Generating Function proper.  This is
-- obviously specific to our idea of an 'Population', takes
-- 'ModelParams' appropriate for it, makes assumptions about the number
-- of genes, and hence takes a specific number of dummy parameters.  To
-- fit a different model, it has to be reimplemented.
--
-- Every arm of this function has to do the same basic things:  add the
-- terminal branches to the total rate, and recursively evaluate the
-- states reachable by coalescing any two genes that are currently in
-- the same deme.
--
-- Additionally, the cases 'Duo', 'Trio', and 'Pure' allow for two demes
-- to join.  The state prior to that is easily constructed by taking the
-- union of the gene sets representing these two demes.  The 'Mixed'
-- case instead invokes 'unmix' to change to 'Pure'.
--
-- The difference between the 'Mixed' and 'Sad' cases is that in the
-- 'Sad' case, the Neandertard doesn't contribute anything.  The only
-- possible event is the extinction of the Neandertard, which leads back
-- to the 'Mixed' case, where the Neandertard genes participate in the
-- regular way.

memoGeneratingFunction :: Floating t => Population -> GFBuild Population ModelParams 79 t
memoGeneratingFunction pop ModelParams{..}  =  gf pop ModelParams{..}
  where
    gf (Solo hom) | isSingleGene hom = ancestralPopulation
                  | otherwise        = terminalBranches hom <> coalescences Solo hom

    gf (Duo           hom pan) = foldMap terminalBranches [hom, pan] <>
                                 coalescences (`Duo` pan) hom <>
                                 coalescences (hom `Duo`) pan <>
                                 exp chimp_split_rate .*. Solo (hom <> pan)

    gf (Trio      sap nea pan) = foldMap terminalBranches [sap, nea, pan] <>
                                 coalescences (\sap' -> Trio sap' nea pan) sap <>
                                 coalescences (\nea' -> Trio sap nea' pan) nea <>
                                 coalescences (\pan' -> Trio sap nea pan') pan <>
                                 exp neand_split_rate .*. Duo (sap <> nea) pan

    gf (Pure  eur afr nea pan) = foldMap terminalBranches [eur, afr, nea, pan] <>
                                 coalescences (\eur' -> Pure eur' afr nea pan) eur <>
                                 coalescences (\afr' -> Pure eur afr' nea pan) afr <>
                                 coalescences (\nea' -> Pure eur afr nea' pan) nea <>
                                 coalescences (\pan' -> Pure eur afr nea pan') pan <>
                                 exp human_split_rate .*. Trio (eur <> afr) nea pan

    gf (Happy eur afr nea pan) = foldMap terminalBranches [eur, afr, nea, pan] <>
                                 coalescences (\eur' -> Pure eur' afr nea pan) eur <>
                                 coalescences (\afr' -> Pure eur afr' nea pan) afr <>
                                 coalescences (\nea' -> Pure eur afr nea' pan) nea <>
                                 coalescences (\pan' -> Pure eur afr nea pan') pan <>
                                 unmix (exp mixing_rate) (\eur' nea' -> Happy eur' afr (nea <> nea') pan) eur <>
                                 exp contact_rate .*. Pure eur afr nea pan

    gf (Mixed eur afr nea pan) = foldMap terminalBranches [eur, afr, nea, pan] <>
                                 coalescences (\eur' -> Pure eur' afr nea pan) eur <>
                                 coalescences (\afr' -> Pure eur afr' nea pan) afr <>
                                 coalescences (\nea' -> Pure eur afr nea' pan) nea <>
                                 coalescences (\pan' -> Pure eur afr nea pan') pan <>
                                 exp admixture_rate .*. Happy eur afr nea pan

    gf (Sad   eur afr nea pan) = foldMap terminalBranches [eur, afr, pan] <>
                                 coalescences (\eur' -> Pure eur' afr nea pan) eur <>
                                 coalescences (\afr' -> Pure eur afr' nea pan) afr <>
                                 coalescences (\pan' -> Pure eur afr nea pan') pan <>
                                 exp extinction_rate .*. Mixed eur afr nea pan


-- XXX  horrible ad-hoc testing below

my_model :: ModelParams Expr
my_model = ModelParams
    { chimp_split_rate   = Var "x_{3}"
    , neand_split_rate   = Var "x_{2}"
    , human_split_rate   = Var "x_{1}"
    , contact_rate       = Var "x_{4}"
    , mixing_rate        = Var "x_{5}"
    , admixture_rate     = Var "x_{6}"
    , extinction_rate    = Var "x_{7}"
    , mutation_rate      = Var "μ" }

-- Approximations of the GFs in the appendix of "Neanderthal Admixture
-- in Eurasia..." to check for correctness.  They should match in
-- spirit; Konrad uses different indices (and absurd notation).  His
-- demes are also permuted, so population splits happen in a different
-- order.

psi_a_slash_b :: Population
psi_a_slash_b = Duo (oneGene 0) (oneGene 1)                                 -- ψ[a/b]

psi_a_b_c :: Population
psi_a_b_c = Solo (oneGene 0 <> oneGene 1 <> oneGene 2)                      -- ψ[a,b,c]

psi_a_slash_bc :: Population
psi_a_slash_bc = Duo (oneGene 0) (coalesce (oneGene 1 <> oneGene 2))        -- ψ[a/{b,c}]

psi_a_slash_b_c :: Population
psi_a_slash_b_c = Duo (oneGene 0) (oneGene 1 <> oneGene 2)                  -- ψ[a/b,c]

psi_a_b_slash_c :: Population
psi_a_b_slash_c = Duo (oneGene 0 <> oneGene 1) (oneGene 2)                  -- ψ[a,b/c]

psi_a_slash_b_slash_c :: Population
psi_a_slash_b_slash_c = Trio (oneGene 0) (oneGene 1) (oneGene 2)            -- ψ[a/b/c]

psi_a_b_slash_slash_c :: Population                                         -- ψ[a,b/Ø/c]
psi_a_b_slash_slash_c = Trio noGene (oneGene 2) (oneGene 0 <> oneGene 1)

psi_a_and_b_slash_slash_c :: Population                                     -- ψ[{a,b}/Ø/c]
psi_a_and_b_slash_slash_c = Trio noGene (oneGene 2) (coalesce $ oneGene 0 <> oneGene 1)


test :: Population -> IO ()
test pp = printE $ generatingFunction memoGeneratingFunction pp my_model omega
  where
    omega = V.generate om
    -- omega = V.generate $ \i -> om $ min i (maxBound - i)
    om i = Var . Name $ "ω_{" <> fromString (show (getFinite i)) <> "}"
