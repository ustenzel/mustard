module Number where

import BasePrelude
import Data.MemoTrie
import GHC.Float

-- | Keep numbers a rational whenever possible so factors can cancel
-- cleanly.  Convert to floating point only when necessary.
data Number = Double !Double | Rat !Rational

r2d :: Rational -> Double
r2d x = fromIntegral (numerator x) / fromIntegral (denominator x)

n2d :: Number -> Double
n2d (Double x) = x
n2d (Rat    x) = r2d x

i2n :: Int -> Number
i2n = Rat . fromIntegral

instance Show Number where
    showsPrec p (Double x) = showsPrec p x
    showsPrec p (Rat    x) = showsPrec p x

instance Eq Number where
    Double a == Double b  =  a == b
    Double a == Rat    b  =  a == r2d b
    Rat    a == Double b  =  r2d a == b
    Rat    a == Rat    b  =  a == b

instance Ord Number where
    Double a `compare` Double b  =  a `compare` b
    Double a `compare` Rat    b  =  a `compare` r2d b
    Rat    a `compare` Double b  =  r2d a `compare` b
    Rat    a `compare` Rat    b  =  a `compare` b

instance Num Number where
    Double a + Double b = Double (a + b)
    Double a + Rat    b = Double (a + r2d b)
    Rat    a + Double b = Double (r2d a + b)
    Rat    a + Rat    b = Rat    (a + b)

    Double a - Double b = Double (a - b)
    Double a - Rat    b = Double (a - r2d b)
    Rat    a - Double b = Double (r2d a - b)
    Rat    a - Rat    b = Rat    (a - b)

    Double a * Double b = Double (a * b)
    Double a * Rat    b = Double (a * r2d b)
    Rat    a * Double b = Double (r2d a * b)
    Rat    a * Rat    b = Rat    (a * b)

    negate (Double a) = Double (negate a)
    negate (Rat    a) = Rat    (negate a)

    abs (Double a) = Double (abs a)
    abs (Rat    a) = Rat    (abs a)

    signum (Double a) = Double (signum a)
    signum (Rat    a) = Rat    (signum a)

    fromInteger = Rat . fromInteger

instance Fractional Number where
    Double a / Double b = Double (a / b)
    Double a / Rat    b = Double (a / r2d b)
    Rat    a / Double b = Double (r2d a / b)
    Rat    a / Rat    b = Rat    (a / b)

    recip (Double a) = Double (recip a)
    recip (Rat    a) = Rat    (recip a)

    fromRational = Rat

instance HasTrie Number where
  data (Number :->: b) = NumberTrie (Word64 :->: b) ((Integer,Integer) :->: b)

  trie f = NumberTrie (trie $ f . Double . castWord64ToDouble) (trie $ f . Rat . uncurry (%))

  untrie (NumberTrie  double_trie _rational_trie) (Double d) = untrie double_trie (castDoubleToWord64 d)
  untrie (NumberTrie _double_trie  rational_trie) (Rat    r) = untrie rational_trie (numerator r, denominator r)

  enumerate (NumberTrie double_trie rational_trie) =
        map (first (Double . castWord64ToDouble)) (enumerate double_trie) ++
        map (first (Rat . uncurry (%))) (enumerate rational_trie)

