{-# LANGUAGE ForeignFunctionInterface #-}
module Main where

import BasePrelude
import Foreign.C.Types                  ( CInt(..) )
import Numeric.AD                       ( grad' )
import Options.Applicative
import System.Time

import qualified Data.HashMap.Strict            as M
import qualified Data.Vector.Storable           as W
import qualified Data.Vector.Storable.Mutable   as WM
import qualified Data.Text.Lazy.Builder         as B
import qualified Data.Text.Lazy.IO              as TL

import Asm
import Bsfs
import Cas

-- XXX  Currently no access to parameters, statistics or return value.
foreign import ccall unsafe "cg_user.h cg_descent"
    cg_descent ::
        Ptr Double ->       -- input: starting guess, output: the solution
        CInt ->             -- problem dimension
        Ptr () ->           -- Stats, structure with statistics (see cg_descent.h)
        Ptr () ->           -- UParm, user parameters, NULL = use default parameters
        Double ->           -- grad_tol, StopRule = 1: |g|_infty <= max (grad_tol, StopFac*initial |g|_infty) [default]
                            --           StopRule = 0: |g|_infty <= grad_tol(1+|f|)
        FunPtr FuncLow ->   -- f = valgrad (g,x,a,n) or f = valgrad (0,x,a,n)
        Ptr Float ->        -- a: argument for f
        Ptr Double ->       -- Work, either size 4n work array or NULL
        IO CInt

minimize :: Double -> W.Vector Double -> FunPtr FuncLow -> Ptr Float -> IO (W.Vector Double)
minimize grad_tol args fun py = do
    x <- W.thaw args
    WM.unsafeWith x $ \px ->
            void $ cg_descent px (fromIntegral $ W.length args) nullPtr nullPtr grad_tol fun py nullPtr
    W.unsafeFreeze x

-- | Invokes dynamically assembled function.
invoke :: FunPtr FuncLow -> Ptr Float -> W.Vector Double -> IO Double
invoke p pa xs = W.unsafeWith xs $ \px -> haskFun p nullPtr px pa (fromIntegral $ W.length xs)

-- | Invokes dynamically assembled function, also returns gradient.
invokeGrad :: FunPtr FuncLow -> Ptr Float -> W.Vector Double -> IO (Double, W.Vector Double)
invokeGrad p pa xs =
    WM.new (W.length xs)                >>= \grad ->
    W.unsafeWith xs                       $ \px ->
    WM.unsafeWith grad                    $ \pg ->
    liftM2 (,) (haskFun p pg px pa (fromIntegral $ W.length xs)) (W.unsafeFreeze grad)


timed :: IO r -> IO r
timed k = do TOD a b <- getClockTime
             r <- k
             TOD c d <- getClockTime
             let t = ((c * 10^(12::Int) + d) - (a * 10^(12::Int) + b)) `div` 10^(9::Int)
             putStrLn $ " (" ++ shows t " ms)"
             return r

putB :: B.Builder -> IO ()
putB = TL.putStrLn . B.toLazyText

main_smoke :: IO ()
main_smoke = do
        -- Log of Rosenbrock banana.  Minimum at (1,1).
        let expr0 [x,y] = log $ square (1 - x) + 100 * square (y - square x)
            expr0 _     = error "expr0: expected exactly two arguments"
            square x    = x * x

            vars        = ["x", "y"]
            (val,grd)   = grad' expr0 $ map (Var . Name) vars
            args        = M.fromList [("x",2), ("y",3)]
            args_vec    = W.fromList [2,3]

        putB $ "expr0 = " <> showE val
        zipWithM_ (\v e -> putB $ "d expr0 / d" <> B.fromText v <> " = " <> showE e) vars grd

        printAsm $ compileExpr expr0 vars

        assemble (compileExpr expr0 vars) $ \p pa -> do
            print $ map (evalExpr args) (val:grd)
            print $ evalFullMemo args (val:grd)
            print $ runEval (mapM evalMemo (val:grd)) args
            print $ evalAsm (compileExpr expr0 vars) args_vec
            print =<< invoke     p pa args_vec
            print =<< invokeGrad p pa args_vec

            print =<< minimize 0.000001 args_vec p pa -- should print [1,1], approximately

main :: IO ()
main = do main' <- execParser full_opts ; main'
  where
    all_opts  = hsubparser $
                   command "bsfs"  (info  bsfs_options     (progDesc "Collects the bsfss of genomes"))
                <> command "fit"   (info  fit_options      (progDesc "Fit parameters to prepared bsfss"))
                <> command "smoke" (info (pure main_smoke) (progDesc "Let the magic smoke out"))

    full_opts = info (all_opts <**> helper)
                     (fullDesc <> progDesc "Test of the Lohse method" <> header "mustard - a test for the Lohse method")

